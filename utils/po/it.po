# Italian translation for apparmor
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the apparmor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: apparmor\n"
"Report-Msgid-Bugs-To: AppArmor list <apparmor@lists.ubuntu.com>\n"
"POT-Creation-Date: 2014-09-14 19:29+0530\n"
"PO-Revision-Date: 2023-01-29 16:22+0000\n"
"Last-Translator: Mark Grassi <Unknown>\n"
"Language-Team: Italian <it@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2023-01-30 06:25+0000\n"
"X-Generator: Launchpad (build 20dbca4abd50eb567cd11c349e7e914443a145a1)\n"
"Language: it\n"

#: ../aa-genprof:56
msgid "Generate profile for the given program"
msgstr "Genera profilo per il programma fornito"

#: ../aa-genprof:57 ../aa-logprof:25 ../aa-cleanprof:24 ../aa-mergeprof:34
#: ../aa-autodep:25 ../aa-audit:25 ../aa-complain:24 ../aa-enforce:24
#: ../aa-disable:24
msgid "path to profiles"
msgstr "percorso ai profili"

#: ../aa-genprof:58 ../aa-logprof:26
msgid "path to logfile"
msgstr "percorso a file registro"

#: ../aa-genprof:59
msgid "name of program to profile"
msgstr "nome del programma per il profilo"

#: ../aa-genprof:69 ../aa-logprof:37
#, python-format
msgid "The logfile %s does not exist. Please check the path"
msgstr "Il file di registro %s non esiste: controllare il percorso"

#: ../aa-genprof:75 ../aa-logprof:43 ../aa-unconfined:36
msgid ""
"It seems AppArmor was not started. Please enable AppArmor and try again."
msgstr ""
"AppArmor non sembra essere stato avviato: abilitare AppArmor e riprovare."

#: ../aa-genprof:80 ../aa-mergeprof:47
#, python-format
msgid "%s is not a directory."
msgstr "%s non è una directory."

#: ../aa-genprof:94
#, python-format
msgid ""
"Can't find %(profiling)s in the system path list. If the name of the "
"application\n"
"is correct, please run 'which %(profiling)s' as a user with correct PATH\n"
"environment set up in order to find the fully-qualified path and\n"
"use the full path as parameter."
msgstr ""
"Impossibile trovare %(profiling)s nell'elenco dei percorsi di sistema. Se il "
"nome\n"
"dell'applicazione è corretto, eseguire \"which %(profiling)s\", come utente "
"con la\n"
"variable PATH impostata correttamente, per trovare il percorso completo\n"
"da utilizzare come parametro."

#: ../aa-genprof:96
#, python-format
msgid "%s does not exists, please double-check the path."
msgstr "%s non esiste: controllare il percorso."

#: ../aa-genprof:124
msgid ""
"\n"
"Before you begin, you may wish to check if a\n"
"profile already exists for the application you\n"
"wish to confine. See the following wiki page for\n"
"more information:"
msgstr ""
"\n"
"Prima di iniziare, verificare se esiste già\n"
"un profilo per l'applicazione da isolare.\n"
"Per maggiori informazioni consultare\n"
"la seguente pagina wiki:"

#: ../aa-genprof:126
msgid ""
"Please start the application to be profiled in\n"
"another window and exercise its functionality now.\n"
"\n"
"Once completed, select the \"Scan\" option below in \n"
"order to scan the system logs for AppArmor events. \n"
"\n"
"For each AppArmor event, you will be given the \n"
"opportunity to choose whether the access should be \n"
"allowed or denied."
msgstr ""
"Si prega di avviare l'applicazione in cui essere profilati\n"
"un'altra finestra ed esercita ora la sua funzionalità.\n"
"\n"
"Una volta completato, seleziona l'opzione \"Scansione\" di seguito in\n"
"per scansionare i registri di sistema per gli eventi AppArmor.\n"
"\n"
"Per ogni evento AppArmor, ti verrà dato il\n"
"possibilità di scegliere se l'accesso deve essere\n"
"consentito o negato."

#: ../aa-genprof:147
msgid "Profiling"
msgstr "Raccolta profilo"

#: ../aa-genprof:165
msgid ""
"\n"
"Reloaded AppArmor profiles in enforce mode."
msgstr ""
"\n"
"Profili AppArmor ricaricati in modalità di applicazione."

#: ../aa-genprof:166
msgid ""
"\n"
"Please consider contributing your new profile!\n"
"See the following wiki page for more information:"
msgstr ""
"\n"
"Per favore, considera di contribuire con il tuo nuovo profilo!\n"
"Vedere la seguente pagina wiki per ulteriori informazioni:"

#: ../aa-genprof:167
#, python-format
msgid "Finished generating profile for %s."
msgstr "Creazione profilo per %s completata."

#: ../aa-logprof:24
msgid "Process log entries to generate profiles"
msgstr "Elabora voci di registro per generare profili"

#: ../aa-logprof:27
msgid "mark in the log to start processing after"
msgstr "contrassegnare nel registro per avviare l'elaborazione dopo"

#: ../aa-cleanprof:23
msgid "Cleanup the profiles for the given programs"
msgstr "Pulisce i profili per i programmi forniti"

#: ../aa-cleanprof:25 ../aa-autodep:26 ../aa-audit:27 ../aa-complain:25
#: ../aa-enforce:25 ../aa-disable:25
msgid "name of program"
msgstr "nome del programma"

#: ../aa-cleanprof:26
msgid "Silently overwrite with a clean profile"
msgstr "Sovrascrivi silenziosamente con un profilo pulito"

#: ../aa-mergeprof:29
msgid "Perform a 2-way or 3-way merge on the given profiles"
msgstr "Esegue unione a 2 o 3 vie sui profili forniti"

#: ../aa-mergeprof:31
msgid "your profile"
msgstr "proprio profilo"

#: ../aa-mergeprof:32
msgid "base profile"
msgstr "profilo base"

#: ../aa-mergeprof:33
msgid "other profile"
msgstr "altro profilo"

#: ../aa-mergeprof:67 ../apparmor/aa.py:2345
msgid ""
"The following local profiles were changed. Would you like to save them?"
msgstr "I seguenti file locali sono stati modificati: salvarli?"

#: ../aa-mergeprof:148 ../aa-mergeprof:430 ../apparmor/aa.py:1767
msgid "Path"
msgstr "Percorso"

#: ../aa-mergeprof:149
msgid "Select the appropriate mode"
msgstr "Seleziona la modalità appropriata"

#: ../aa-mergeprof:166
msgid "Unknown selection"
msgstr "Selezione sconosciuta"

#: ../aa-mergeprof:183 ../aa-mergeprof:209
msgid "File includes"
msgstr "Il file include"

#: ../aa-mergeprof:183 ../aa-mergeprof:209
msgid "Select the ones you wish to add"
msgstr "Selezionare quelli da aggiungere"

#: ../aa-mergeprof:195 ../aa-mergeprof:222
#, python-format
msgid "Adding %s to the file."
msgstr "Aggiunta di %s al file"

#: ../aa-mergeprof:199 ../apparmor/aa.py:2258
msgid "unknown"
msgstr "sconosciuto"

#: ../aa-mergeprof:224 ../aa-mergeprof:275 ../aa-mergeprof:516
#: ../aa-mergeprof:558 ../aa-mergeprof:675 ../apparmor/aa.py:1620
#: ../apparmor/aa.py:1859 ../apparmor/aa.py:1899 ../apparmor/aa.py:2012
#, python-format
msgid "Deleted %s previous matching profile entries."
msgstr "Eliminate %s voci del profilo corrispondenti precedenti."

#: ../aa-mergeprof:244 ../aa-mergeprof:429 ../aa-mergeprof:629
#: ../aa-mergeprof:656 ../apparmor/aa.py:992 ../apparmor/aa.py:1252
#: ../apparmor/aa.py:1562 ../apparmor/aa.py:1603 ../apparmor/aa.py:1766
#: ../apparmor/aa.py:1958 ../apparmor/aa.py:1994
msgid "Profile"
msgstr "Profilo"

#: ../aa-mergeprof:245 ../apparmor/aa.py:1563 ../apparmor/aa.py:1604
msgid "Capability"
msgstr "Capacità"

#: ../aa-mergeprof:246 ../aa-mergeprof:480 ../apparmor/aa.py:1258
#: ../apparmor/aa.py:1564 ../apparmor/aa.py:1605 ../apparmor/aa.py:1817
msgid "Severity"
msgstr "Severità"

#: ../aa-mergeprof:273 ../aa-mergeprof:514 ../apparmor/aa.py:1618
#: ../apparmor/aa.py:1857
#, python-format
msgid "Adding %s to profile."
msgstr "Aggiunta di %s al profilo."

#: ../aa-mergeprof:282 ../apparmor/aa.py:1627
#, python-format
msgid "Adding capability %s to profile."
msgstr "Aggiunta capacità %s al profilo."

#: ../aa-mergeprof:289 ../apparmor/aa.py:1634
#, python-format
msgid "Denying capability %s to profile."
msgstr "Negare la capacità %s al profilo."

#: ../aa-mergeprof:439 ../aa-mergeprof:470 ../apparmor/aa.py:1776
#: ../apparmor/aa.py:1807
msgid "(owner permissions off)"
msgstr "(autorizzazioni del proprietario disattivate)"

#: ../aa-mergeprof:444 ../apparmor/aa.py:1781
msgid "(force new perms to owner)"
msgstr "(forzare i nuovi permessi al proprietario)"

#: ../aa-mergeprof:447 ../apparmor/aa.py:1784
msgid "(force all rule perms to owner)"
msgstr "(forza tutti i permessi delle regole al proprietario)"

#: ../aa-mergeprof:459 ../apparmor/aa.py:1796
msgid "Old Mode"
msgstr "Modalità precedente"

#: ../aa-mergeprof:460 ../apparmor/aa.py:1797
msgid "New Mode"
msgstr "Modalità nuova"

#: ../aa-mergeprof:475 ../apparmor/aa.py:1812
msgid "(force perms to owner)"
msgstr "(forzare i permessi al proprietario)"

#: ../aa-mergeprof:478 ../apparmor/aa.py:1815
msgid "Mode"
msgstr "Modalità"

#: ../aa-mergeprof:556
#, python-format
msgid "Adding %(path)s %(mod)s to profile"
msgstr "Aggiunta di %(path)s %(mod)s al profilo"

#: ../aa-mergeprof:574 ../apparmor/aa.py:1915
msgid "Enter new path: "
msgstr "Inserire nuovo percorso: "

#: ../aa-mergeprof:630 ../aa-mergeprof:657 ../apparmor/aa.py:1959
#: ../apparmor/aa.py:1995
msgid "Network Family"
msgstr "Famiglia di rete"

#: ../aa-mergeprof:631 ../aa-mergeprof:658 ../apparmor/aa.py:1960
#: ../apparmor/aa.py:1996
msgid "Socket Type"
msgstr "Tipo di socket"

#: ../aa-mergeprof:673 ../apparmor/aa.py:2010
#, python-format
msgid "Adding %s to profile"
msgstr "Aggiunta di %s al profilo"

#: ../aa-mergeprof:683 ../apparmor/aa.py:2020
#, python-format
msgid "Adding network access %(family)s %(type)s to profile."
msgstr "Aggiunta dell'accesso alla rete %(family)s %(type)s al profilo."

#: ../aa-mergeprof:689 ../apparmor/aa.py:2026
#, python-format
msgid "Denying network access %(family)s %(type)s to profile"
msgstr "Negare l'accesso alla rete %(family)s %(type)s al profilo"

#: ../aa-autodep:23
msgid "Generate a basic AppArmor profile by guessing requirements"
msgstr "Genera un profilo AppArmor di base indovinando i requisiti"

#: ../aa-autodep:24
msgid "overwrite existing profile"
msgstr "sovrascrivere il profilo esistente"

#: ../aa-audit:24
msgid "Switch the given programs to audit mode"
msgstr "Passa i programmi forniti in modalità di controllo"

#: ../aa-audit:26
msgid "remove audit mode"
msgstr "rimuovere la modalità di controllo"

#: ../aa-audit:28
msgid "Show full trace"
msgstr "Mostra traccia completa"

#: ../aa-complain:23
msgid "Switch the given program to complain mode"
msgstr "Passa il programma indicato in modalità reclamo"

#: ../aa-enforce:23
msgid "Switch the given program to enforce mode"
msgstr "Passa il programma indicato in modalità reclamo"

#: ../aa-disable:23
msgid "Disable the profile for the given programs"
msgstr "Disabilita il profilo per i programmi indicati"

#: ../aa-unconfined:28
msgid "Lists unconfined processes having tcp or udp ports"
msgstr "Elenca i processi non confinati con porte TCP o UDP"

#: ../aa-unconfined:29
msgid "scan all processes from /proc"
msgstr "scansiona tutti i processi da /proc"

#: ../aa-unconfined:81
#, python-format
msgid "%(pid)s %(program)s (%(commandline)s) not confined"
msgstr "%(pid)s %(program)s (%(commandline)s) non limitato"

#: ../aa-unconfined:85
#, python-format
msgid "%(pid)s %(program)s%(pname)s not confined"
msgstr "%(pid)s %(program)s%(pname)s non limitato"

#: ../aa-unconfined:90
#, python-format
msgid "%(pid)s %(program)s (%(commandline)s) confined by '%(attribute)s'"
msgstr "%(pid)s %(program)s (%(commandline)s) delimitato da '%(attribute)s'"

#: ../aa-unconfined:94
#, python-format
msgid "%(pid)s %(program)s%(pname)s confined by '%(attribute)s'"
msgstr "%(pid)s %(program)s%(pname)s limitato da '%(attribute)s'"

#: ../apparmor/aa.py:196
#, python-format
msgid "Followed too many links while resolving %s"
msgstr "Ho seguito troppi link durante la risoluzione di %s"

#: ../apparmor/aa.py:252 ../apparmor/aa.py:259
#, python-format
msgid "Can't find %s"
msgstr "Impossibile trovare %s"

#: ../apparmor/aa.py:264 ../apparmor/aa.py:548
#, python-format
msgid "Setting %s to complain mode."
msgstr "Impostazione di %s in modalità reclamo."

#: ../apparmor/aa.py:271
#, python-format
msgid "Setting %s to enforce mode."
msgstr "Impostazione di %s per la modalità forzata."

#: ../apparmor/aa.py:286
#, python-format
msgid "Unable to find basename for %s."
msgstr "Impossibile trovare il nome di base per %s."

#: ../apparmor/aa.py:301
#, python-format
msgid "Could not create %(link)s symlink to %(filename)s."
msgstr ""
"Impossibile creare il collegamento simbolico di %(link)s a %(filename)s."

#: ../apparmor/aa.py:314
#, python-format
msgid "Unable to read first line from %s: File Not Found"
msgstr "Impossibile leggere la prima riga da %s: File non trovato"

#: ../apparmor/aa.py:328
#, python-format
msgid ""
"Unable to fork: %(program)s\n"
"\t%(error)s"
msgstr ""
"Impossibile eseguire il fork: %(program)s\n"
"\t%(error)s"

#: ../apparmor/aa.py:449 ../apparmor/ui.py:303
msgid ""
"Are you sure you want to abandon this set of profile changes and exit?"
msgstr ""
"Sei sicuro di voler abbandonare questo insieme di modifiche al profilo e "
"uscire?"

#: ../apparmor/aa.py:451 ../apparmor/ui.py:305
msgid "Abandoning all changes."
msgstr "Abbandonare tutte le modifiche."

#: ../apparmor/aa.py:464
msgid "Connecting to repository..."
msgstr "Connessione al repository..."

#: ../apparmor/aa.py:470
msgid "WARNING: Error fetching profiles from the repository"
msgstr "ATTENZIONE: errore durante il recupero dei profili dal repository"

#: ../apparmor/aa.py:550
#, python-format
msgid "Error activating profiles: %s"
msgstr "Errore durante l'attivazione dei profili: %s"

#: ../apparmor/aa.py:605
#, python-format
msgid "%s contains no profile"
msgstr "%s non contiene alcun profilo"

#: ../apparmor/aa.py:706
#, python-format
msgid ""
"WARNING: Error synchronizing profiles with the repository:\n"
"%s\n"
msgstr ""
"ATTENZIONE: Errore durante la sincronizzazione dei profili con il "
"repository:\n"
"%s\n"

#: ../apparmor/aa.py:744
#, python-format
msgid ""
"WARNING: Error synchronizing profiles with the repository\n"
"%s"
msgstr ""
"ATTENZIONE: Errore durante la sincronizzazione dei profili con il "
"repository\n"
"%s"

#: ../apparmor/aa.py:832 ../apparmor/aa.py:883
#, python-format
msgid ""
"WARNING: An error occurred while uploading the profile %(profile)s\n"
"%(ret)s"
msgstr ""
"ATTENZIONE: si è verificato un errore durante il caricamento del profilo "
"%(profile)s\n"
"%(ret)s"

#: ../apparmor/aa.py:833
msgid "Uploaded changes to repository."
msgstr "Modifiche caricate nel repository."

#: ../apparmor/aa.py:865
msgid "Changelog Entry: "
msgstr "Voce del registro delle modifiche: "

#: ../apparmor/aa.py:885
msgid ""
"Repository Error\n"
"Registration or Signin was unsuccessful. User login\n"
"information is required to upload profiles to the repository.\n"
"These changes could not be sent."
msgstr ""
"Errore di repository\n"
"La registrazione o l'accesso non sono riusciti. Login utente\n"
"le informazioni sono necessarie per caricare i profili nel repository.\n"
"Impossibile inviare queste modifiche."

#: ../apparmor/aa.py:995
msgid "Default Hat"
msgstr "Cappello predefinito"

#: ../apparmor/aa.py:997
msgid "Requested Hat"
msgstr "Cappello richiesto"

#: ../apparmor/aa.py:1218
#, python-format
msgid "%s has transition name but not transition mode"
msgstr "%s ha il nome della transizione ma non la modalità di transizione"

#: ../apparmor/aa.py:1232
#, python-format
msgid "Target profile exists: %s\n"
msgstr "Il profilo di destinazione esiste: %s\n"

#: ../apparmor/aa.py:1254
msgid "Program"
msgstr "Programma"

#: ../apparmor/aa.py:1257
msgid "Execute"
msgstr "Esegui"

#: ../apparmor/aa.py:1287
msgid "Are you specifying a transition to a local profile?"
msgstr "Stai specificando una transizione a un profilo locale?"

#: ../apparmor/aa.py:1299
msgid "Enter profile name to transition to: "
msgstr "Inserisci il nome del profilo a cui passare: "

#: ../apparmor/aa.py:1308
msgid ""
"Should AppArmor sanitise the environment when\n"
"switching profiles?\n"
"\n"
"Sanitising environment is more secure,\n"
"but some applications depend on the presence\n"
"of LD_PRELOAD or LD_LIBRARY_PATH."
msgstr ""
"AppArmor dovrebbe sanificare l'ambiente quando\n"
"cambiare profilo?\n"
"\n"
"La sanificazione dell'ambiente è più sicura,\n"
"ma alcune applicazioni dipendono dalla presenza\n"
"di LD_PRELOAD o LD_LIBRARY_PATH."

#: ../apparmor/aa.py:1310
msgid ""
"Should AppArmor sanitise the environment when\n"
"switching profiles?\n"
"\n"
"Sanitising environment is more secure,\n"
"but this application appears to be using LD_PRELOAD\n"
"or LD_LIBRARY_PATH and sanitising the environment\n"
"could cause functionality problems."
msgstr ""
"AppArmor dovrebbe sanificare l'ambiente quando\n"
"cambiare profilo?\n"
"\n"
"La sanificazione dell'ambiente è più sicura,\n"
"ma questa applicazione sembra utilizzare LD_PRELOAD\n"
"o LD_LIBRARY_PATH e sanificando l'ambiente\n"
"potrebbe causare problemi di funzionalità."

#: ../apparmor/aa.py:1318
#, python-format
msgid ""
"Launching processes in an unconfined state is a very\n"
"dangerous operation and can cause serious security holes.\n"
"\n"
"Are you absolutely certain you wish to remove all\n"
"AppArmor protection when executing %s ?"
msgstr ""
"Avviare processi in uno stato non confinato è molto\n"
"operazione pericolosa e può causare gravi falle di sicurezza.\n"
"\n"
"Sei assolutamente certo di voler rimuovere tutto\n"
"Protezione AppArmor durante l'esecuzione di %s?"

#: ../apparmor/aa.py:1320
msgid ""
"Should AppArmor sanitise the environment when\n"
"running this program unconfined?\n"
"\n"
"Not sanitising the environment when unconfining\n"
"a program opens up significant security holes\n"
"and should be avoided if at all possible."
msgstr ""
"AppArmor dovrebbe sanificare l'ambiente quando\n"
"eseguire questo programma non confinato?\n"
"\n"
"Non igienizzare l'ambiente durante il deconfinamento\n"
"un programma apre significative falle di sicurezza\n"
"e dovrebbe essere evitato se possibile."

#: ../apparmor/aa.py:1396 ../apparmor/aa.py:1414
#, python-format
msgid ""
"A profile for %s does not exist.\n"
"Do you want to create one?"
msgstr ""
"Non esiste un profilo per %s.\n"
"Vuoi crearne uno?"

#: ../apparmor/aa.py:1523
msgid "Complain-mode changes:"
msgstr "Modifiche alla modalità di reclamo:"

#: ../apparmor/aa.py:1525
msgid "Enforce-mode changes:"
msgstr "Modifiche alla modalità di applicazione:"

#: ../apparmor/aa.py:1528
#, python-format
msgid "Invalid mode found: %s"
msgstr "Modalità non valida trovata: %s"

#: ../apparmor/aa.py:1897
#, python-format
msgid "Adding %(path)s %(mode)s to profile"
msgstr "Aggiunta di %(path)s %(mode)s al profilo"

#: ../apparmor/aa.py:1918
#, python-format
msgid ""
"The specified path does not match this log entry:\n"
"\n"
"  Log Entry: %(path)s\n"
"  Entered Path:  %(ans)s\n"
"Do you really want to use this path?"
msgstr ""
"Il percorso specificato non corrisponde a questa voce di registro:\n"
"\n"
"  Voce di registro: %(path)s\n"
"  Percorso inserito: %(ans)s\n"
"Vuoi davvero usare questo percorso?"

#: ../apparmor/aa.py:2251
#, python-format
msgid "Reading log entries from %s."
msgstr "Lettura delle voci di registro da %s."

#: ../apparmor/aa.py:2254
#, python-format
msgid "Updating AppArmor profiles in %s."
msgstr "Aggiornamento dei profili AppArmor in %s."

#: ../apparmor/aa.py:2323
msgid ""
"Select which profile changes you would like to save to the\n"
"local profile set."
msgstr ""
"Seleziona le modifiche al profilo in cui desideri salvare\n"
"set di profili locali."

#: ../apparmor/aa.py:2324
msgid "Local profile changes"
msgstr "Modifiche al profilo locale"

#: ../apparmor/aa.py:2418
msgid "Profile Changes"
msgstr "Modifiche al profilo"

#: ../apparmor/aa.py:2428
#, python-format
msgid "Can't find existing profile %s to compare changes."
msgstr ""
"Impossibile trovare il profilo esistente %s per confrontare le modifiche."

#: ../apparmor/aa.py:2566 ../apparmor/aa.py:2581
#, python-format
msgid "Can't read AppArmor profiles in %s"
msgstr "Impossibile leggere i profili AppArmor in %s"

#: ../apparmor/aa.py:2677
#, python-format
msgid ""
"%(profile)s profile in %(file)s contains syntax errors in line: %(line)s."
msgstr ""
"Il profilo %(profile)s in %(file)s contiene errori di sintassi nella riga: "
"%(line)s."

#: ../apparmor/aa.py:2734
#, python-format
msgid ""
"Syntax Error: Unexpected End of Profile reached in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: Fine del profilo imprevista raggiunta nel file: %(file)s "
"line: %(line)s"

#: ../apparmor/aa.py:2749
#, python-format
msgid ""
"Syntax Error: Unexpected capability entry found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: voce di capacità imprevista trovata nel file: %(file)s "
"line: %(line)s"

#: ../apparmor/aa.py:2770
#, python-format
msgid ""
"Syntax Error: Unexpected link entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: trovata una voce di collegamento imprevista nel file: "
"%(file)s line: %(line)s"

#: ../apparmor/aa.py:2798
#, python-format
msgid ""
"Syntax Error: Unexpected change profile entry found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: è stata trovata una voce di profilo di modifica "
"imprevista nel file: %(file)s line: %(line)s"

#: ../apparmor/aa.py:2820
#, python-format
msgid ""
"Syntax Error: Unexpected rlimit entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: voce rlimit imprevista trovata nel file: %(file)s line: "
"%(line)s"

#: ../apparmor/aa.py:2831
#, python-format
msgid ""
"Syntax Error: Unexpected boolean definition found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: trovata una definizione booleana imprevista nel file: "
"%(file)s line: %(line)s"

#: ../apparmor/aa.py:2871
#, python-format
msgid ""
"Syntax Error: Unexpected bare file rule found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: regola del file bare imprevista trovata nel file: "
"%(file)s line: %(line)s"

#: ../apparmor/aa.py:2894
#, python-format
msgid ""
"Syntax Error: Unexpected path entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: nel file è stata trovata una voce di percorso "
"imprevista: %(file)s line: %(line)s"

#: ../apparmor/aa.py:2922
#, python-format
msgid "Syntax Error: Invalid Regex %(path)s in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: espressione regolare non valida %(path)s nel file: riga "
"%(file)s: %(line)s"

#: ../apparmor/aa.py:2925
#, python-format
msgid "Invalid mode %(mode)s in file: %(file)s line: %(line)s"
msgstr "Modalità non valida %(mode)s nel file: %(file)s line: %(line)s"

#: ../apparmor/aa.py:2977
#, python-format
msgid ""
"Syntax Error: Unexpected network entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: voce di rete imprevista trovata nel file: %(file)s line: "
"%(line)s"

#: ../apparmor/aa.py:3007
#, python-format
msgid ""
"Syntax Error: Unexpected dbus entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: voce dbus imprevista trovata nel file: %(file)s line: "
"%(line)s"

#: ../apparmor/aa.py:3030
#, python-format
msgid ""
"Syntax Error: Unexpected mount entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: voce di montaggio imprevista trovata nel file: %(file)s "
"line: %(line)s"

#: ../apparmor/aa.py:3052
#, python-format
msgid ""
"Syntax Error: Unexpected signal entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: nel file è stata trovata una voce di segnale imprevista: "
"%(file)s line: %(line)s"

#: ../apparmor/aa.py:3074
#, python-format
msgid ""
"Syntax Error: Unexpected ptrace entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: voce ptrace non prevista trovata nel file: %(file)s "
"line: %(line)s"

#: ../apparmor/aa.py:3096
#, python-format
msgid ""
"Syntax Error: Unexpected pivot_root entry found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: voce pivot_root non prevista trovata nel file: %(file)s "
"line: %(line)s"

#: ../apparmor/aa.py:3118
#, python-format
msgid ""
"Syntax Error: Unexpected unix entry found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: trovata una voce unix imprevista nel file: %(file)s "
"line: %(line)s"

#: ../apparmor/aa.py:3140
#, python-format
msgid ""
"Syntax Error: Unexpected change hat declaration found in file: %(file)s "
"line: %(line)s"
msgstr ""
"Errore di sintassi: dichiarazione del cappello di modifica imprevista "
"trovata nel file: %(file)s line: %(line)s"

#: ../apparmor/aa.py:3152
#, python-format
msgid ""
"Syntax Error: Unexpected hat definition found in file: %(file)s line: "
"%(line)s"
msgstr ""
"Errore di sintassi: trovata una definizione del cappello imprevista nel "
"file: %(file)s line: %(line)s"

#: ../apparmor/aa.py:3168
#, python-format
msgid "Error: Multiple definitions for hat %(hat)s in profile %(profile)s."
msgstr ""
"Errore: più definizioni per il cappello %(hat)s nel profilo %(profile)s."

#: ../apparmor/aa.py:3185
#, python-format
msgid "Warning: invalid \"REPOSITORY:\" line in %s, ignoring."
msgstr "Avviso: riga \"REPOSITORY:\" non valida in %s, ignorando."

#: ../apparmor/aa.py:3198
#, python-format
msgid "Syntax Error: Unknown line found in file: %(file)s line: %(line)s"
msgstr ""
"Errore di sintassi: riga sconosciuta trovata nel file: riga %(file)s: "
"%(line)s"

#: ../apparmor/aa.py:3211
#, python-format
msgid ""
"Syntax Error: Missing '}' or ','. Reached end of file %(file)s while inside "
"profile %(profile)s"
msgstr ""
"Errore di sintassi: '}' o ',' mancanti. È stata raggiunta la fine del file "
"%(file)s all'interno del profilo %(profile)s"

#: ../apparmor/aa.py:3277
#, python-format
msgid "Redefining existing variable %(variable)s: %(value)s in %(file)s"
msgstr ""
"Ridefinizione della variabile esistente %(variable)s: %(value)s in %(file)s"

#: ../apparmor/aa.py:3282
#, python-format
msgid ""
"Values added to a non-existing variable %(variable)s: %(value)s in %(file)s"
msgstr ""
"Valori aggiunti a una variabile inesistente %(variable)s: %(value)s in "
"%(file)s"

#: ../apparmor/aa.py:3284
#, python-format
msgid ""
"Unknown variable operation %(operation)s for variable %(variable)s in "
"%(file)s"
msgstr ""
"Operazione variabile sconosciuta %(operation)s per la variabile %(variable)s "
"in %(file)s"

#: ../apparmor/aa.py:3343
#, python-format
msgid "Invalid allow string: %(allow)s"
msgstr "Stringa di autorizzazione non valida: %(allow)s"

#: ../apparmor/aa.py:3778
msgid "Can't find existing profile to modify"
msgstr "Impossibile trovare il profilo esistente da modificare"

#: ../apparmor/aa.py:4347
#, python-format
msgid "Writing updated profile for %s."
msgstr "Scrittura del profilo aggiornato per %s."

#: ../apparmor/aa.py:4481
#, python-format
msgid "File Not Found: %s"
msgstr "File non trovato: %s"

#: ../apparmor/aa.py:4591
#, python-format
msgid ""
"%s is currently marked as a program that should not have its own\n"
"profile.  Usually, programs are marked this way if creating a profile for \n"
"them is likely to break the rest of the system.  If you know what you're\n"
"doing and are certain you want to create a profile for this program, edit\n"
"the corresponding entry in the [qualifiers] section in "
"/etc/apparmor/logprof.conf."
msgstr ""
"%s è attualmente contrassegnato come un programma che non dovrebbe avere il "
"proprio\n"
"profilo. Di solito, i programmi sono contrassegnati in questo modo se si "
"crea un profilo per\n"
"è probabile che rompano il resto del sistema. Se sai cosa sei\n"
"facendo e sei certo di voler creare un profilo per questo programma, "
"modifica\n"
"la voce corrispondente nella sezione [qualifiers] in "
"/etc/apparmor/logprof.conf."

#: ../apparmor/logparser.py:127 ../apparmor/logparser.py:132
#, python-format
msgid "Log contains unknown mode %s"
msgstr "Il registro contiene la modalità sconosciuta %s"

#: ../apparmor/tools.py:84 ../apparmor/tools.py:126
#, python-format
msgid ""
"Can't find %(program)s in the system path list. If the name of the "
"application\n"
"is correct, please run 'which %(program)s' as a user with correct PATH\n"
"environment set up in order to find the fully-qualified path and\n"
"use the full path as parameter."
msgstr ""
"Impossibile trovare %(program)s nell'elenco dei percorsi di sistema. Se il "
"nome dell'applicazione\n"
"è corretto, eseguire 'quale %(program)s' come utente con PERCORSO corretto\n"
"ambiente predisposto per trovare il percorso pienamente qualificato e\n"
"utilizzare il percorso completo come parametro."

#: ../apparmor/tools.py:86 ../apparmor/tools.py:102 ../apparmor/tools.py:128
#, python-format
msgid "%s does not exist, please double-check the path."
msgstr "%s non esiste, per favore ricontrolla il percorso."

#: ../apparmor/tools.py:100
msgid ""
"The given program cannot be found, please try with the fully qualified path "
"name of the program: "
msgstr ""
"Impossibile trovare il programma indicato, provare con il nome del percorso "
"completo del programma: "

#: ../apparmor/tools.py:113 ../apparmor/tools.py:137 ../apparmor/tools.py:157
#: ../apparmor/tools.py:175 ../apparmor/tools.py:193
#, python-format
msgid "Profile for %s not found, skipping"
msgstr "Profilo per %s non trovato, sto saltando"

#: ../apparmor/tools.py:140
#, python-format
msgid "Disabling %s."
msgstr "Disattivazione di %s."

#: ../apparmor/tools.py:198
#, python-format
msgid "Setting %s to audit mode."
msgstr "Impostazione di %s in modalità di controllo."

#: ../apparmor/tools.py:200
#, python-format
msgid "Removing audit mode from %s."
msgstr "Rimozione della modalità di controllo da %s."

#: ../apparmor/tools.py:212
#, python-format
msgid ""
"Please pass an application to generate a profile for, not a profile itself - "
"skipping %s."
msgstr ""
"Passa un'applicazione per la generazione di un profilo, non un profilo "
"stesso, saltando %s."

#: ../apparmor/tools.py:220
#, python-format
msgid "Profile for %s already exists - skipping."
msgstr "Il profilo per %s esiste già - sto saltando."

#: ../apparmor/tools.py:232
#, python-format
msgid ""
"\n"
"Deleted %s rules."
msgstr ""
"\n"
"%s regole eliminate."

#: ../apparmor/tools.py:240
#, python-format
msgid ""
"The local profile for %(program)s in file %(file)s was changed. Would you "
"like to save it?"
msgstr ""
"Il profilo locale per %(program)s nel file %(file)s è stato modificato. Vuoi "
"salvarlo?"

#: ../apparmor/tools.py:260
#, python-format
msgid "The profile for %s does not exists. Nothing to clean."
msgstr "Il profilo per %s non esiste. Niente da pulire."

#: ../apparmor/ui.py:61
msgid "Invalid hotkey for"
msgstr "Tasto di scelta rapida non valido per"

#: ../apparmor/ui.py:77 ../apparmor/ui.py:121 ../apparmor/ui.py:275
msgid "(Y)es"
msgstr "(S)i"

#: ../apparmor/ui.py:78 ../apparmor/ui.py:122 ../apparmor/ui.py:276
msgid "(N)o"
msgstr "(N)o"

#: ../apparmor/ui.py:123
msgid "(C)ancel"
msgstr "(C)ancella"

#: ../apparmor/ui.py:223
msgid "(A)llow"
msgstr "(C)onsenti"

#: ../apparmor/ui.py:224
msgid "(M)ore"
msgstr "(A)ltro"

#: ../apparmor/ui.py:225
msgid "Audi(t)"
msgstr "(V)erifica"

#: ../apparmor/ui.py:226
msgid "Audi(t) off"
msgstr "Audi(t) off"

#: ../apparmor/ui.py:227
msgid "Audit (A)ll"
msgstr "Verifica (T)utto"

#: ../apparmor/ui.py:229
msgid "(O)wner permissions on"
msgstr "(P)ermessi proprietario attivate"

#: ../apparmor/ui.py:230
msgid "(O)wner permissions off"
msgstr "(P)ermessi proprietario disattivate"

#: ../apparmor/ui.py:231
msgid "(D)eny"
msgstr "Negat(o)"

#: ../apparmor/ui.py:232
msgid "Abo(r)t"
msgstr "Fa(l)lito"

#: ../apparmor/ui.py:233
msgid "(F)inish"
msgstr "Te(r)minato"

#: ../apparmor/ui.py:234
msgid "(I)nherit"
msgstr "(E)reditato"

#: ../apparmor/ui.py:235
msgid "(P)rofile"
msgstr "(P)rofilo"

#: ../apparmor/ui.py:236
msgid "(P)rofile Clean Exec"
msgstr "(P)rofilo Pulito Exec"

#: ../apparmor/ui.py:237
msgid "(C)hild"
msgstr "(F)iglio"

#: ../apparmor/ui.py:238
msgid "(C)hild Clean Exec"
msgstr ""

#: ../apparmor/ui.py:239
msgid "(N)amed"
msgstr ""

#: ../apparmor/ui.py:240
msgid "(N)amed Clean Exec"
msgstr ""

#: ../apparmor/ui.py:241
msgid "(U)nconfined"
msgstr "Non conf(i)nato"

#: ../apparmor/ui.py:242
msgid "(U)nconfined Clean Exec"
msgstr ""

#: ../apparmor/ui.py:243
msgid "(P)rofile Inherit"
msgstr ""

#: ../apparmor/ui.py:244
msgid "(P)rofile Inherit Clean Exec"
msgstr ""

#: ../apparmor/ui.py:245
msgid "(C)hild Inherit"
msgstr ""

#: ../apparmor/ui.py:246
msgid "(C)hild Inherit Clean Exec"
msgstr ""

#: ../apparmor/ui.py:247
msgid "(N)amed Inherit"
msgstr ""

#: ../apparmor/ui.py:248
msgid "(N)amed Inherit Clean Exec"
msgstr ""

#: ../apparmor/ui.py:249
msgid "(X) ix On"
msgstr ""

#: ../apparmor/ui.py:250
msgid "(X) ix Off"
msgstr ""

#: ../apparmor/ui.py:251 ../apparmor/ui.py:265
msgid "(S)ave Changes"
msgstr ""

#: ../apparmor/ui.py:252
msgid "(C)ontinue Profiling"
msgstr ""

#: ../apparmor/ui.py:253
msgid "(N)ew"
msgstr ""

#: ../apparmor/ui.py:254
msgid "(G)lob"
msgstr ""

#: ../apparmor/ui.py:255
msgid "Glob with (E)xtension"
msgstr ""

#: ../apparmor/ui.py:256
msgid "(A)dd Requested Hat"
msgstr ""

#: ../apparmor/ui.py:257
msgid "(U)se Default Hat"
msgstr ""

#: ../apparmor/ui.py:258
msgid "(S)can system log for AppArmor events"
msgstr ""

#: ../apparmor/ui.py:259
msgid "(H)elp"
msgstr ""

#: ../apparmor/ui.py:260
msgid "(V)iew Profile"
msgstr ""

#: ../apparmor/ui.py:261
msgid "(U)se Profile"
msgstr ""

#: ../apparmor/ui.py:262
msgid "(C)reate New Profile"
msgstr ""

#: ../apparmor/ui.py:263
msgid "(U)pdate Profile"
msgstr ""

#: ../apparmor/ui.py:264
msgid "(I)gnore Update"
msgstr ""

#: ../apparmor/ui.py:266
msgid "Save Selec(t)ed Profile"
msgstr "Salva (p)rofilo selezionato"

#: ../apparmor/ui.py:267
msgid "(U)pload Changes"
msgstr "(C)arica modifiche"

#: ../apparmor/ui.py:268
msgid "(V)iew Changes"
msgstr "(V)isualizza modifiche"

#: ../apparmor/ui.py:269
msgid "View Changes b/w (C)lean profiles"
msgstr ""

#: ../apparmor/ui.py:270
msgid "(V)iew"
msgstr ""

#: ../apparmor/ui.py:271
msgid "(E)nable Repository"
msgstr ""

#: ../apparmor/ui.py:272
msgid "(D)isable Repository"
msgstr ""

#: ../apparmor/ui.py:273
msgid "(N)ever Ask Again"
msgstr ""

#: ../apparmor/ui.py:274
msgid "Ask Me (L)ater"
msgstr ""

#: ../apparmor/ui.py:277
msgid "Allow All (N)etwork"
msgstr ""

#: ../apparmor/ui.py:278
msgid "Allow Network Fa(m)ily"
msgstr ""

#: ../apparmor/ui.py:279
msgid "(O)verwrite Profile"
msgstr ""

#: ../apparmor/ui.py:280
msgid "(K)eep Profile"
msgstr ""

#: ../apparmor/ui.py:281
msgid "(C)ontinue"
msgstr ""

#: ../apparmor/ui.py:282
msgid "(I)gnore"
msgstr ""

#: ../apparmor/ui.py:344
#, python-format
msgid "PromptUser: Unknown command %s"
msgstr ""

#: ../apparmor/ui.py:351
#, python-format
msgid "PromptUser: Duplicate hotkey for %(command)s: %(menutext)s "
msgstr ""

#: ../apparmor/ui.py:363
msgid "PromptUser: Invalid hotkey in default item"
msgstr ""

#: ../apparmor/ui.py:368
#, python-format
msgid "PromptUser: Invalid default %s"
msgstr ""
